﻿
using System.Runtime.InteropServices;

namespace LED_FX_SDK.SDKs.Corsair.Mapper
{
    // ReSharper disable once InconsistentNaming    
    /// <summary>
    /// CUE-SDK: contains information about led and its color
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class CorsairLedColor
    {
        /// <summary>
        /// CUE-SDK: identifier of LED to set
        /// </summary>
        internal int ledId;

        /// <summary>
        /// CUE-SDK: red   brightness[0..255]
        /// </summary>
        internal int r;

        /// <summary>
        /// CUE-SDK: green brightness[0..255]
        /// </summary>
        internal int g;

        /// <summary>
        /// CUE-SDK: blue  brightness[0..255]
        /// </summary>
        internal int b;

        public CorsairLedId LedId
        {
            get
            {
                return (CorsairLedId)ledId;
            }
            set
            {
                ledId = (int)value;
            }
        }
    };
}
