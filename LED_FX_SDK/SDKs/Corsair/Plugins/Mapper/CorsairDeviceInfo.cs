﻿using System;
using System.Runtime.InteropServices;

namespace LED_FX_SDK.SDKs.Corsair.Mapper
{
    [StructLayout(LayoutKind.Sequential)]
    public class CorsairDeviceInfo
    {
        /// <summary>
        /// CUE-SDK: enum describing device type
        /// </summary>
        private CorsairDeviceType type;
        public CorsairDeviceType Type => type;

        /// <summary>
        /// CUE-SDK: null - terminated device model(like “K95RGB”)
        /// </summary>
        private IntPtr model;
        public string Model => Marshal.PtrToStringAnsi(model);

        /// <summary>
        /// CUE-SDK: enum describing physical layout of the keyboard or mouse
        /// </summary>
        private int physicalLayout;
        public int PhysicalLayout => physicalLayout;

        /// <summary>
        /// CUE-SDK: enum describing logical layout of the keyboard as set in CUE settings
        /// </summary>
        private int logicalLayout;
        public int LogicalLayout => logicalLayout;

        /// <summary>
        /// CUE-SDK: mask that describes device capabilities, formed as logical “or” of CorsairDeviceCaps enum values
        /// </summary>
        private int capsMask;
        public int CapsMask => capsMask;
        /// <summary>
        /// CUE-SDK: number of controllable LEDs on the device
        /// </summary>
        private int ledsCount;
        public int LedsCount => ledsCount;
    }
}
