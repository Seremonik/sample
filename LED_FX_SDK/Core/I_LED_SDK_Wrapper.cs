﻿using LED_FX_SDK.Core;
using System;
using UnityEngine;

namespace LED_FX_SDK.SDKs
{
    public interface I_LED_SDK_Wrapper
    {
        void Initialize(Action OnSuccess, Action<LED_SDK_Exception> OnError);
        LED_SDK_Exception Release();
        bool IsInitialized();

        void Refresh();
        void SetGlobalColor(Color color);
        void SetKeyColor(KeyCode keyCode, Color color);
    }
}
