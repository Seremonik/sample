﻿using System.Runtime.InteropServices;
using System.Text;

namespace AlienFXManagedWrapper
{
    public class AlienFXWrapperMap
    {
        #region Properties
        public const string DLL_NAME = "LightFX";
        #endregion

        #region Methods
        // **************************************
        // *** Basic and recommended functions **
        // **************************************
        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_Initialize();

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_Release();

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_Reset();

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_Update();

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_Light(LFX_Position locationMask, uint lightCol);

        // ********************************************
        // *** Functions for extended functionality ***
        // ********************************************
        // Just for compatibility. It has no functionality on most of the platforms
        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_UpdateDefault();

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_GetNumDevices(out uint numDevices);

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_GetDeviceDescription(uint devIndex, StringBuilder devDesc, int devDescSize, out LFX_DeviceType devType);

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_GetNumLights(uint devIndex, out uint numLights);

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_GetLightDescription(uint devIndex, uint lightIndex, StringBuilder lightDesc, int devDescSize);

        // Added just for campatibility. It has no functionality on most of the platforms
        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_GetLightLocation(uint devIndex, uint lightIndex, out LFX_Position lightLoc);

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_GetLightColor(uint devIndex, uint lightIndex, ref LFX_ColorStruct lightCol);

        [DllImport(DLL_NAME)]
        public static extern LFX_Result LFX_SetLightColor(uint devIndex, uint lightIndex, ref LFX_ColorStruct lightCol);
        #endregion
    }
}
