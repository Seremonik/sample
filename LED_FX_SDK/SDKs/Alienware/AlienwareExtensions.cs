﻿using AlienFXManagedWrapper;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Alienware.Extensions
{
    public static class AlienwareExtensions
    {
        public static uint ToUint(this Color color)
        {
            Color32 c = color;
            return (uint)(((c.a << 24) | (c.r << 16) | (c.g << 8) | c.b) & 0xffffffffL);
        }

        public static LFX_ColorStruct ToColorStruct(this Color color)
        {
            LFX_ColorStruct colorStruct = new LFX_ColorStruct()
            {
                red = (byte)(color.r * 255),
                green = (byte)(color.g * 255),
                blue = (byte)(color.b * 255),
                brightness = (byte)(color.a * 255)
            };
            return colorStruct;
        }
    }
}
