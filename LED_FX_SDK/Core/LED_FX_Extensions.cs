﻿using UnityEngine;

namespace LED_FX_SDK.Core
{
    public static partial class LED_FX_Extensions
    {
        #region Public methods

        /// <summary>
        /// Return color with given alpha value
        /// </summary>
        /// <param name="color">Given color</param>
        /// <param name="alpha">New alpha value</param>
        /// <returns></returns>
        public static Color SetAlpha(this Color color, float alpha)
        {
            return new Color(color.r, color.g, color.b, alpha);
        }

        #endregion Public methods
    }
}
