﻿namespace LED_FX_SDK.Core
{
    /// <summary>
    /// Class to inherits from when creating own Animation effects.
    /// Its handled by the list in LED_FX_Controller
    /// </summary>
    public class LED_FX_Animation
    {
        /// <summary>
        /// Read only property to check if Animation has Finished
        /// </summary>
        public bool HasFinished => !IsLooped && durationLeft <= 0;

        /// <summary>
        /// Total duration of the animation since its creation. Can be changed only by custom animation
        /// </summary>
        public float TotalDuration { protected set; get; }

        /// <summary>
        /// Property to check if animation is paused
        /// </summary>
        public bool IsPaused { private set; get; } = false;

        /// <summary>
        /// Property to check id animation is looped. Can be changed by custom animation
        /// </summary>
        public bool IsLooped { protected set; get; } = false;


        protected float delayLeft = 0;
        protected float durationLeft = float.MaxValue;

        /// <summary>
        /// Animation Update. Its invoked in LED_FX_Controller_Loop
        /// </summary>
        /// <param name="deltaTime">difference in time between two Animation updates</param>
        public void AnimationUpdate(float deltaTime)
        {
            if(!IsPaused && !HasFinished)
            {
                if(delayLeft <= 0)
                {
                    Animation(deltaTime);

                    if(!IsLooped)
                    {
                        durationLeft -= deltaTime;
                    }
                    TotalDuration += deltaTime;
                }
                else
                {
                    delayLeft -= deltaTime;
                }
            }
        }

        /// <summary>
        /// Stops the animation, LED_Controller will remove this animation after that from the list
        /// </summary>
        public void Stop()
        {
            IsLooped = false;
            durationLeft = 0;
        }

        /// <summary>
        /// Pause or Resume the animation depeding on current state
        /// </summary>
        public void PauseToggle()
        {
            if(IsPaused)
            {
                Resume();
            }
            else
            {
                Pause();
            }
        }

        /// <summary>
        /// Pause the animation leaving it in the list
        /// </summary>
        public void Pause()
        {
            IsPaused = true;
            AnimationPaused();
        }

        /// <summary>
        /// Resumes the animation
        /// </summary>
        public void Resume()
        {
            IsPaused = false;
            AnimationResumed();
        }

        /// <summary>
        /// Action called when Animation is removed from the list. After Invoking Stop
        /// </summary>
        public virtual void AnimationEnded() { }

        /// <summary>
        /// Action called when animation is added to the list
        /// </summary>
        public virtual void AnimationStarted() { }

        /// <summary>
        /// Action called when animation is Paused
        /// </summary>
        public virtual void AnimationPaused() { }

        /// <summary>
        /// Action called when animation is Resumed
        /// </summary>
        public virtual void AnimationResumed() { }

        /// <summary>
        /// Main animation. Its invoked every Animation Update
        /// </summary>
        /// <param name="deltaTime">Difference in time between two Animation updates</param>
        protected virtual void Animation(float deltaTime) { }
    }
}
