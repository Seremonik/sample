﻿namespace LED_FX_SDK.SDKs.Corsair.Mapper
{
    public enum CorsairDeviceType
    {
        Unknown = 0,
        Mouse = 1,
        Keyboard = 2,
        Headset = 3,
        Mousemat = 4,
        HeadsetStand = 5
    };
}
