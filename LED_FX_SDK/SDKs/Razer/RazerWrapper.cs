﻿using Colore;
using LED_FX_SDK.Core;
using LED_FX_SDK.SDKs.Razer.Extensions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Razer
{
    public class RazerWrapper : I_LED_SDK_Wrapper
    {
        private bool isInitialized = false;
        private bool keysChanges = false;
        private bool firstInit = true;
        private Dictionary<KeyCode, UnityEngine.Color> keyColors = new Dictionary<KeyCode, Color>();

        private IChroma chroma;
        private Action onSuccess;
        private Action<LED_SDK_Exception> onError;

        #region Public methods

        public void Initialize(Action OnSuccess, Action<LED_SDK_Exception> OnError)
        {
            onSuccess = OnSuccess;
            onError = OnError;
            Initialize();
        }

        public LED_SDK_Exception Release()
        {
            if(IsInitialized())
            {
                try
                {
                    ReleaseAsync();
                }
                catch(Exception e)
                {
                    return new LED_SDK_Exception(LED_SDK_ERROR_CODE.UNKNOWN_ERROR, "Razer Chroma Threw exception during Release: " + e.ToString());
                }
            }
            return new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, "");
        }

        public bool IsInitialized()
        {
            return isInitialized;
        }

        public void SetGlobalColor(UnityEngine.Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    Task.Factory.StartNew(() => SetGlobalColorTask(color.ToColore()));
                }
                catch(Exception) { }
            }
        }

        public void SetKeyColor(KeyCode keyCode, UnityEngine.Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    lock(keyColors)
                    {
                        keyColors[keyCode] = color;
                    }
                    keysChanges = true;
                }
                catch(Exception) { }
            }
        }

        public void Refresh()
        {
            if(keysChanges)
            {
                Task.Factory.StartNew(SetKeyTask);
                keysChanges = false;
            }
        }

        #endregion

        #region Private methods

        private async void Initialize()
        {
            try
            {
                chroma = await ColoreProvider.CreateNativeAsync();
                isInitialized = chroma.Initialized;
                if(isInitialized)
                {
                    if(firstInit)
                    {
                        await Task.Delay(TimeSpan.FromMilliseconds(1000));
                        firstInit = false;
                    }
                    keyColors.Clear();
                    SetGlobalColor(Color.black);
                    onSuccess();
                    return;
                }
                onError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Razer SDK failed to initialized"));
                return;
            }
            catch(Exception e)
            {
                onError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Razer Chroma SDK initalized with errors: " + e.ToString()));
                return;
            }

        }

        private async void ReleaseAsync()
        {
            chroma.Unregister();
            await chroma.UninitializeAsync();
        }

        private async void SetKeyTask()
        {
            try
            {
                foreach(var keyColor in keyColors)
                {
                    await chroma.Keyboard.SetKeyAsync(keyColor.Key.ToKey(), keyColor.Value.ToColore());
                }
                keyColors.Clear();
            }
            catch(Exception) { }
        }

        private async void SetGlobalColorTask(Colore.Data.Color color)
        {
            try
            {
                await chroma.SetAllAsync(color);
            }
            catch(Exception) { }
        }

        #endregion
    }
}
