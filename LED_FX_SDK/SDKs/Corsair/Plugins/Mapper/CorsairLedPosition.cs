﻿using System.Runtime.InteropServices;

namespace LED_FX_SDK.SDKs.Corsair.Mapper
{
    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// CUE-SDK: contains led id and position of led rectangle.Most of the keys are rectangular.
    /// In case if key is not rectangular(like Enter in ISO / UK layout) it returns the smallest rectangle that fully contains the key
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class CorsairLedPosition
    {
        /// <summary>
        /// CUE-SDK: identifier of led
        /// </summary>
        internal CorsairLedId ledId;

        /// <summary>
        /// CUE-SDK: values in mm
        /// </summary>
        internal double top;

        /// <summary>
        /// CUE-SDK: values in mm
        /// </summary>
        internal double left;

        /// <summary>
        /// CUE-SDK: values in mm
        /// </summary>
        internal double height;

        /// <summary>
        /// CUE-SDK: values in mm
        /// </summary>
        internal double width;
    }
}
