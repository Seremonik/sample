﻿using LED_FX_SDK.SDKs;
using LED_FX_SDK.SDKs.Alienware;
using LED_FX_SDK.SDKs.Asus;
using LED_FX_SDK.SDKs.Corsair;
using LED_FX_SDK.SDKs.Logitech;
using LED_FX_SDK.SDKs.Razer;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

namespace LED_FX_SDK.Core
{
    /// <summary>
    /// Class to control LEDs of all suported devices. There should be only one class, thats why its a Singleton to avoid multiple hardware SDKs initialization.
    /// </summary>
    public class LED_FX_Controller
    {
        #region Variables

        /// <summary>
        /// Sets time between color refresh. eg. 1/40 = 40FPS. 40FPS seems to be optimal for all devices.
        /// </summary>
        public static float RefresRate = 1f / 40f;
        public readonly static LED_FX_Animation_Wrapper Animations = new LED_FX_Animation_Wrapper();

        private List<I_LED_SDK_Wrapper> availableSDKs;

        private Dictionary<KeyCode, Color> keyColors;
        private List<KeyCode> keyCodesToClear;
        private List<LED_FX_Animation> animations;

        private bool keysColorHasChanged = false;
        private bool colorHasChanged = false;
        private bool isRazerSupported;
        private bool isLogitechSupported;
        private bool isCorsairSupported;
        private bool isAsusSupported;
        private bool isAlienwareSupported;

        private float timeSinceRefresh = float.MaxValue;
        private Color ambientColor = Color.black;
        private Color currentGlobalColor = Color.black;

        #endregion Variables

        #region Properties

        /// <summary>
        /// Checks if Controller is initlaized
        /// </summary>
        public static bool IsInitialized
        {
            get;
            private set;
        } = false;

        /// <summary>
        /// Checks if Controller is in debug mode
        /// </summary>
        public bool IsDebuggingEnabled
        {
            get;
            private set;
        } = false;

        /// <summary>
        /// private variable for property <see cref="Instance"/>. Use property to get/set value.
        /// </summary>
        private static LED_FX_Controller instance = null;

        /// <summary>
        /// Instance property of LED_FX_Controller
        /// </summary>
        public static LED_FX_Controller Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new LED_FX_Controller();
                }
                return instance;
            }
        }

        /// <summary>
        /// Sets Ambient Color which is overwritten by global one
        /// </summary>
        public static Color AmbientColor
        {
            get { return Instance.ambientColor; }
            set { SetAmbientColor(value); }
        }

        /// <summary>
        /// Sets Global Color which is always over ambient one
        /// </summary>
        public static Color GlobalColor
        {
            get { return Instance.currentGlobalColor; }
            set { SetGlobalColor(value); }
        }

        /// <summary>
        /// Return actual color of the devices after blending Global with Ambient
        /// </summary>
        public static Color CurrentDevicesColor => Color.Lerp(AmbientColor, GlobalColor, GlobalColor.a);

        /// <summary>
        /// Gets info about currently lit keys and colors
        /// </summary>
        public static ReadOnlyDictionary<KeyCode, Color> KeyColors => new ReadOnlyDictionary<KeyCode, Color>(Instance.keyColors);

        #endregion Properties

        #region Constructors

        private LED_FX_Controller()
        {
            availableSDKs = new List<I_LED_SDK_Wrapper>();
            keyColors = new Dictionary<KeyCode, Color>();
            animations = new List<LED_FX_Animation>();
            keyCodesToClear = new List<KeyCode>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Initialize all SDK and checks for available devices.
        /// </summary>
        /// <param name="isDebuggingEnabled">Flag for enabling debugging mode</param>
        public static void Initialize(bool isRazerSupported, bool isAlienwareSupported, bool isAsusSupported, bool isCorsairSupported, bool isLogitechSupported, bool isDebuggingEnabled = false)
        {
            Instance.isRazerSupported = isRazerSupported;
            Instance.isAlienwareSupported = isAlienwareSupported;
            Instance.isAsusSupported = isAsusSupported;
            Instance.isCorsairSupported = isCorsairSupported;
            Instance.isLogitechSupported = isLogitechSupported;

            if(!IsInitialized)
            {
                Instance.InitializeValues();
                IsInitialized = isDebuggingEnabled;
                Instance.IsDebuggingEnabled = isDebuggingEnabled;

                foreach(var sdk in Instance.availableSDKs)
                {
                    sdk.Initialize(InitializationSuccess, LogException);
                }
                ResetToAmbient();
            }
        }

        /// <summary>
        /// Release the hardware back to the system. Should be invoked before application close.
        /// </summary>
        public static void Release()
        {
            if(IsInitialized)
            {
                foreach(var sdk in Instance.availableSDKs)
                {
                    LED_SDK_Exception exception = sdk.Release();
                    LogException(exception);
                }
                IsInitialized = false;
            }
        }

        /// <summary>
        /// In order to make any animations this function must be invoked every frame.
        /// </summary>
        public static void Update(float deltaTime)
        {
            if(IsInitialized)
            {
                if(Instance.timeSinceRefresh >= RefresRate)
                {
                    HandleAnimations(Instance.timeSinceRefresh);
                    Instance.timeSinceRefresh = 0;

                    if(Instance.colorHasChanged)
                    {
                        Refresh();
                        Instance.colorHasChanged = false;
                    }
                    else if(Instance.keysColorHasChanged)
                    {
                        RefreshKeys();
                        Instance.keysColorHasChanged = false;

                        foreach(var keycode in Instance.keyCodesToClear)
                        {
                            Instance.keyColors.Remove(keycode);
                        }
                        Instance.keyCodesToClear.Clear();
                    }
                }
                Instance.timeSinceRefresh += deltaTime;
            }
        }

        /// <summary>
        /// Sets One Color to the list of keys.
        /// </summary>
        /// <param name="color">Color to change keys to.</param>
        /// <param name="keyCodes">Array of Unity key codes to change color.</param>
        public static void SetKeysColor(Color color, params KeyCode[] keyCodes)
        {
            if(IsInitialized)
            {
                foreach(var keyCode in keyCodes)
                {
                    if((!Instance.keyColors.ContainsKey(keyCode) || Instance.keyColors[keyCode] != color))
                    {
                        Instance.keyColors[keyCode] = color;
                        Instance.keysColorHasChanged = true;
                    }
                }
            }
        }

        /// <summary>
        /// Resets colors to Ambient or a default one.
        /// </summary>
        /// <param name="clearKeys">If set to true, all the keys will be cleared. Otherwise keys will override ambient color.</param>
        public static void ResetToAmbient(bool clearKeys = false)
        {
            if(IsInitialized && Instance.currentGlobalColor != Color.black.SetAlpha(0))
            {
                Instance.currentGlobalColor = Color.black.SetAlpha(0);
                if(clearKeys)
                {
                    Instance.keyColors.Clear();
                }
                Instance.colorHasChanged = true;
                Instance.keysColorHasChanged = true;
            }
        }

        /// <summary>
        /// Clears all keys color.
        /// </summary>
        public static void ClearAllKeys()
        {
            if(IsInitialized && Instance.keyColors.Count > 0)
            {
                Instance.keyColors.Clear();
                Instance.colorHasChanged = true;
            }
        }

        /// <summary>
        /// Clears Colors for given keys
        /// </summary>
        /// <param name="keyCodes"></param>
        public static void ClearKeys(params KeyCode[] keyCodes)
        {
            if(IsInitialized)
            {
                //Checks if we can clear all keys
                bool clearAll = false;
                if(Instance.keyColors.Count == keyCodes.Length)
                {
                    clearAll = true;
                    for(int i = 0; i < keyCodes.Length && clearAll; i++)
                    {
                        clearAll &= Instance.keyColors.ContainsKey(keyCodes[i]);
                    }
                }

                if(clearAll)
                {
                    ClearAllKeys();
                }
                else
                {
                    foreach(var keyCode in keyCodes)
                    {
                        if(Instance.keyColors.ContainsKey(keyCode) && Instance.keyColors[keyCode] != Instance.ambientColor)
                        {
                            Instance.keyColors[keyCode] = Color.Lerp(Instance.ambientColor, Instance.currentGlobalColor, Instance.currentGlobalColor.a);
                        }
                    }

                    Instance.keyCodesToClear.AddRange(keyCodes);
                    Instance.keysColorHasChanged = true;
                }
            }
        }

        /// <summary>
        /// Adds new animation to the animation list
        /// </summary>
        /// <param name="animation"></param>
        public void AddAnimation(LED_FX_Animation animation)
        {
            if(IsInitialized)
            {
                animation.AnimationStarted();
                animations.Insert(0, animation);
            }
        }

        #endregion

        #region Private methods

        private void InitializeValues()
        {
            availableSDKs.Clear();

            if(Instance.isRazerSupported)
            {
                availableSDKs.Add(new RazerWrapper());
            }
            if(Instance.isAsusSupported)
            {
                availableSDKs.Add(new AsusWrapper());
            }
            if(Instance.isAlienwareSupported)
            {
                availableSDKs.Add(new AlienwareWrapper());
            }
            if(Instance.isCorsairSupported)
            {
                availableSDKs.Add(new CorsairWrapper());
            }
            if(Instance.isLogitechSupported)
            {
                availableSDKs.Add(new LogitechWrapper());
            }

            keyColors.Clear();
            keyCodesToClear.Clear();
            animations.Clear();

            keysColorHasChanged = false;
            colorHasChanged = false;

            timeSinceRefresh = float.MaxValue;
            ambientColor = Color.black;
            currentGlobalColor = Color.black;
        }

        private static void InitializationSuccess()
        {
            IsInitialized = true;
        }


        private static void LogException(LED_SDK_Exception exception)
        {
            if(Instance.IsDebuggingEnabled && exception.errorCode != LED_SDK_ERROR_CODE.OK)
            {
                Debug.Log(exception);
            }
        }

        private static void RefreshKeys()
        {
            Color backgroundColor = Color.Lerp(Instance.ambientColor, Instance.currentGlobalColor, Instance.currentGlobalColor.a);

            foreach(var sdk in Instance.availableSDKs)
            {
                if(sdk.IsInitialized())
                {
                    //Set Keys to given colors
                    foreach(var keyColors in Instance.keyColors)
                    {
                        sdk.SetKeyColor(keyColors.Key, Color.Lerp(backgroundColor, keyColors.Value, keyColors.Value.a));
                    }
                    if(Instance.keyColors.Count > 0)
                    {
                        sdk.Refresh();
                    }
                }
            }
        }

        private static void Refresh()
        {
            foreach(var sdk in Instance.availableSDKs)
            {
                if(sdk.IsInitialized())
                {
                    sdk.SetGlobalColor(Color.Lerp(Instance.ambientColor, Instance.currentGlobalColor, Instance.currentGlobalColor.a));

                    sdk.Refresh();
                }
            }
        }

        private static void HandleAnimations(float deltaTime)
        {
            for(int i = Instance.animations.Count - 1; i >= 0; i--)
            {
                if(Instance.animations[i] == null)
                {
                    Instance.animations.RemoveAt(i);
                }
                else if(Instance.animations[i].HasFinished)
                {
                    Instance.animations[i].AnimationEnded();
                    Instance.animations.RemoveAt(i);
                }
                else
                {
                    Instance.animations[i].AnimationUpdate(deltaTime);
                }
            }
        }

        private static void SetGlobalColor(Color color)
        {
            if(IsInitialized && Instance.currentGlobalColor != color)
            {
                Instance.currentGlobalColor = color;

                Instance.colorHasChanged = true;
                Instance.keysColorHasChanged = true;
            }
        }

        private static void SetAmbientColor(Color color)
        {
            if(IsInitialized && color != Instance.ambientColor)
            {
                Instance.ambientColor = color;
                Instance.colorHasChanged = true;
                Instance.keysColorHasChanged = true;
            }
        }

        #endregion
    }

    #region Helper classes

    /// <summary>
    /// Helper class for syntactic sugar to hold all Animations under one variable
    /// </summary>
    public class LED_FX_Animation_Wrapper
    {
        /// <summary>
        /// Adds animation to LED Controller
        /// </summary>
        /// <param name="animation">Created animation</param>
        /// <returns>Added animation</returns>
        public LED_FX_Animation AddAnimation(LED_FX_Animation animation)
        {
            LED_FX_Controller.Instance.AddAnimation(animation);
            return animation;
        }
    }

    #endregion Helper classes
}
