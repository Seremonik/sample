﻿
using System.Text;

namespace LED_FX_SDK.Core
{
    public enum LED_SDK_ERROR_CODE
    {
        OK,
        SDK_UNAVAILABLE,
        SDK_NOT_INITIALIZED,
        NO_DEVICES,
        UNKNOWN_ERROR,
    }

    public struct LED_SDK_Exception
    {
        public LED_SDK_ERROR_CODE errorCode;
        public string description;

        public LED_SDK_Exception(LED_SDK_ERROR_CODE errorCode, string description)
        {
            this.errorCode = errorCode;
            this.description = description;
        }

        public LED_SDK_Exception(LED_SDK_ERROR_CODE errorCode) : this(errorCode, "") { }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("LED SDK Exception with code: ");
            stringBuilder.AppendLine(errorCode.ToString());
            stringBuilder.Append("Description: ");
            stringBuilder.Append(description);

            return stringBuilder.ToString();
        }
    }
}
