﻿using System;
using System.Runtime.InteropServices;

namespace LED_FX_SDK.SDKs.Corsair.Mapper
{
    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// CUE-SDK: contains number of leds and arrays with their positions
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    public class CorsairLedPositions
    {
        /// <summary>
        /// CUE-SDK: integer value.Number of elements in following array
        /// </summary>
        internal int numberOfLed;

        /// <summary>
        /// CUE-SDK: array of led positions
        /// </summary>
        private IntPtr pLedPosition;

        public CorsairLedPosition[] LedPosition
        {
            get
            {
                CorsairLedPosition[] ledPositionArray = new CorsairLedPosition[numberOfLed];
                int structSize = Marshal.SizeOf(typeof(CorsairLedPosition));
                IntPtr ptr = pLedPosition;
                for(int i = 0; i < numberOfLed; i++)
                {
                    CorsairLedPosition ledPosition = (CorsairLedPosition)Marshal.PtrToStructure(ptr, typeof(CorsairLedPosition));

                    ptr = new IntPtr(ptr.ToInt64() + structSize);
                    ledPositionArray[i] = ledPosition;
                }
                return ledPositionArray;
            }
        }

    }
}
