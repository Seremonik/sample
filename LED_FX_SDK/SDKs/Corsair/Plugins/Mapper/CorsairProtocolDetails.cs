﻿using System;
using System.Runtime.InteropServices;

namespace LED_FX_SDK.SDKs.Corsair.Mapper
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CorsairProtocolDetails
    {
        /// <summary>
        /// CUE-SDK: null - terminated string containing version of SDK(like “1.0.0.1”). Always contains valid value even if there was no CUE found
        /// </summary>
        private IntPtr sdkVersion;
        public string SdkVersion => Marshal.PtrToStringAnsi(sdkVersion);
        /// <summary>
        /// CUE-SDK: null - terminated string containing version of CUE(like “1.0.0.1”) or NULL if CUE was not found.
        /// </summary>
        private IntPtr serverVersion;
        public string ServerVersion => Marshal.PtrToStringAnsi(serverVersion);
        public bool IsSDKAvailable => serverVersion != IntPtr.Zero;

        /// <summary>
        /// CUE-SDK: integer number that specifies version of protocol that is implemented by current SDK.
        /// Numbering starts from 1. Always contains valid value even if there was no CUE found
        /// </summary>
        private int sdkProtocolVersion;
        public int SdkProtocolVersion => sdkProtocolVersion;

        /// <summary>
        /// CUE-SDK: integer number that specifies version of protocol that is implemented by CUE.
        /// Numbering starts from 1. If CUE was not found then this value will be 0
        /// </summary>
        private int serverProtocolVersion;
        public int ServerProtocolVersion => serverProtocolVersion;

        /// <summary>
        /// CUE-SDK: boolean value that specifies if there were breaking changes between version of protocol implemented by server and client
        /// </summary>
        private byte breakingChanges;
        public bool BreakingChanges => Convert.ToBoolean(breakingChanges);
    };
}
