﻿using UnityEngine;

namespace LED_FX_SDK.Core
{
    /// <summary>
    /// Extension class only for Syntactic sugar purpose. Instead of LED_FX_Controller.AddAnimation(new FlashAnimation(...)) you can invoke it LED_FX_Controller.Animations.Flash(...)
    /// </summary>
    public static partial class LED_FX_Extensions
    {
        public static LED_FX_Animation Pulse(this LED_FX_Animation_Wrapper animationWrapper, Color color, AnimationCurve curve)
        {
            return animationWrapper.AddAnimation(new PulseAnimation(color, curve));
        }

        public static LED_FX_Animation PulseKey(this LED_FX_Animation_Wrapper animationWrapper, Color color, KeyCode keyCode, AnimationCurve curve)
        {
            return animationWrapper.AddAnimation(new PulseKeyAnimation(color, keyCode, curve));
        }
    }

    public class PulseAnimation : LED_FX_Animation
    {
        Color color;
        AnimationCurve curve;

        public PulseAnimation(Color color, AnimationCurve curve)
        {
            this.curve = curve;
            this.color = color;
            IsLooped = true;
        }

        protected override void Animation(float deltaTime)
        {
            LED_FX_Controller.GlobalColor = (color.SetAlpha(curve.Evaluate(TotalDuration)));
        }

        public override void AnimationEnded()
        {
            LED_FX_Controller.ResetToAmbient();
        }
    }

    public class PulseKeyAnimation : LED_FX_Animation
    {
        Color color;
        KeyCode keycode;
        AnimationCurve curve;

        public PulseKeyAnimation(Color color, KeyCode keycode, AnimationCurve curve)
        {
            this.curve = curve;
            this.keycode = keycode;
            this.color = color;
            IsLooped = true;
        }

        protected override void Animation(float deltaTime)
        {
            LED_FX_Controller.SetKeysColor(color.SetAlpha(curve.Evaluate(TotalDuration)), keycode);
        }

        public override void AnimationPaused()
        {
            LED_FX_Controller.ClearKeys(keycode);
        }

        public override void AnimationEnded()
        {
            LED_FX_Controller.ClearKeys(keycode);
        }
    }
}
