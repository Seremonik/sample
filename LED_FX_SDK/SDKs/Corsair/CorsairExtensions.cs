﻿using LED_FX_SDK.SDKs.Corsair.Mapper;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Corsair.Extensions
{
    public static class CorsairExtensions
    {
        public static CorsairLedId ToCorsairLedId(this KeyCode keycode)
        {
            switch(keycode)
            {
                case KeyCode.None:
                    return CorsairLedId.Invalid;
                case KeyCode.Backspace:
                    return CorsairLedId.Backspace;
                case KeyCode.Delete:
                    return CorsairLedId.Delete;
                case KeyCode.Tab:
                    return CorsairLedId.Tab;
                case KeyCode.Clear:
                    return CorsairLedId.Delete;
                case KeyCode.Return:
                    return CorsairLedId.Enter;
                case KeyCode.Pause:
                    return CorsairLedId.PauseBreak;
                case KeyCode.Escape:
                    return CorsairLedId.Escape;
                case KeyCode.Space:
                    return CorsairLedId.Space;
                case KeyCode.Keypad0:
                    return CorsairLedId.Keypad0;
                case KeyCode.Keypad1:
                    return CorsairLedId.Keypad1;
                case KeyCode.Keypad2:
                    return CorsairLedId.Keypad2;
                case KeyCode.Keypad3:
                    return CorsairLedId.Keypad3;
                case KeyCode.Keypad4:
                    return CorsairLedId.Keypad4;
                case KeyCode.Keypad5:
                    return CorsairLedId.Keypad5;
                case KeyCode.Keypad6:
                    return CorsairLedId.Keypad6;
                case KeyCode.Keypad7:
                    return CorsairLedId.Keypad7;
                case KeyCode.Keypad8:
                    return CorsairLedId.Keypad8;
                case KeyCode.Keypad9:
                    return CorsairLedId.Keypad9;
                case KeyCode.KeypadPeriod:
                    return CorsairLedId.KeypadPeriodAndDelete;
                case KeyCode.KeypadDivide:
                    return CorsairLedId.KeypadSlash;
                case KeyCode.KeypadMultiply:
                    return CorsairLedId.KeypadAsterisk;
                case KeyCode.KeypadMinus:
                    return CorsairLedId.KeypadMinus;
                case KeyCode.KeypadPlus:
                    return CorsairLedId.KeypadPlus;
                case KeyCode.KeypadEnter:
                    return CorsairLedId.KeypadEnter;
                case KeyCode.KeypadEquals:
                    return CorsairLedId.KeypadEnter;
                case KeyCode.UpArrow:
                    return CorsairLedId.UpArrow;
                case KeyCode.DownArrow:
                    return CorsairLedId.DownArrow;
                case KeyCode.RightArrow:
                    return CorsairLedId.RightArrow;
                case KeyCode.LeftArrow:
                    return CorsairLedId.LeftArrow;
                case KeyCode.Insert:
                    return CorsairLedId.Insert;
                case KeyCode.Home:
                    return CorsairLedId.Home;
                case KeyCode.End:
                    return CorsairLedId.End;
                case KeyCode.PageUp:
                    return CorsairLedId.PageUp;
                case KeyCode.PageDown:
                    return CorsairLedId.PageDown;
                case KeyCode.F1:
                    return CorsairLedId.F1;
                case KeyCode.F2:
                    return CorsairLedId.F2;
                case KeyCode.F3:
                    return CorsairLedId.F3;
                case KeyCode.F4:
                    return CorsairLedId.F4;
                case KeyCode.F5:
                    return CorsairLedId.F5;
                case KeyCode.F6:
                    return CorsairLedId.F6;
                case KeyCode.F7:
                    return CorsairLedId.F7;
                case KeyCode.F8:
                    return CorsairLedId.F8;
                case KeyCode.F9:
                    return CorsairLedId.F9;
                case KeyCode.F10:
                    return CorsairLedId.F10;
                case KeyCode.F11:
                    return CorsairLedId.F11;
                case KeyCode.F12:
                    return CorsairLedId.F12;
                case KeyCode.F13:
                    return CorsairLedId.Invalid;
                case KeyCode.F14:
                    return CorsairLedId.Invalid;
                case KeyCode.F15:
                    return CorsairLedId.Invalid;
                case KeyCode.Alpha0:
                    return CorsairLedId.D0;
                case KeyCode.Alpha1:
                    return CorsairLedId.D1;
                case KeyCode.Alpha2:
                    return CorsairLedId.D2;
                case KeyCode.Alpha3:
                    return CorsairLedId.D3;
                case KeyCode.Alpha4:
                    return CorsairLedId.D4;
                case KeyCode.Alpha5:
                    return CorsairLedId.D5;
                case KeyCode.Alpha6:
                    return CorsairLedId.D6;
                case KeyCode.Alpha7:
                    return CorsairLedId.D7;
                case KeyCode.Alpha8:
                    return CorsairLedId.D8;
                case KeyCode.Alpha9:
                    return CorsairLedId.D9;
                case KeyCode.Exclaim:
                    return CorsairLedId.D0;
                case KeyCode.DoubleQuote:
                    return CorsairLedId.ApostropheAndDoubleQuote;
                case KeyCode.Hash:
                    return CorsairLedId.D3;
                case KeyCode.Dollar:
                    return CorsairLedId.D4;
                case KeyCode.Percent:
                    return CorsairLedId.D5;
                case KeyCode.Ampersand:
                    return CorsairLedId.D7;
                case KeyCode.Quote:
                    return CorsairLedId.ApostropheAndDoubleQuote;
                case KeyCode.LeftParen:
                    return CorsairLedId.D9;
                case KeyCode.RightParen:
                    return CorsairLedId.D0;
                case KeyCode.Asterisk:
                    return CorsairLedId.D8;
                case KeyCode.Plus:
                    return CorsairLedId.EqualsAndPlus;
                case KeyCode.Comma:
                    return CorsairLedId.CommaAndLessThan;
                case KeyCode.Minus:
                    return CorsairLedId.MinusAndUnderscore;
                case KeyCode.Period:
                    return CorsairLedId.PeriodAndBiggerThan;
                case KeyCode.Slash:
                    return CorsairLedId.SlashAndQuestionMark;
                case KeyCode.Colon:
                    return CorsairLedId.SemicolonAndColon;
                case KeyCode.Semicolon:
                    return CorsairLedId.SemicolonAndColon;
                case KeyCode.Less:
                    return CorsairLedId.CommaAndLessThan;
                case KeyCode.Equals:
                    return CorsairLedId.EqualsAndPlus;
                case KeyCode.Greater:
                    return CorsairLedId.PeriodAndBiggerThan;
                case KeyCode.Question:
                    return CorsairLedId.SlashAndQuestionMark;
                case KeyCode.At:
                    return CorsairLedId.D2;
                case KeyCode.LeftBracket:
                    return CorsairLedId.BracketLeft;
                case KeyCode.Backslash:
                    return CorsairLedId.Backslash;
                case KeyCode.RightBracket:
                    return CorsairLedId.BracketRight;
                case KeyCode.Caret:
                    return CorsairLedId.D6;
                case KeyCode.Underscore:
                    return CorsairLedId.MinusAndUnderscore;
                case KeyCode.BackQuote:
                    return CorsairLedId.GraveAccentAndTilde;
                case KeyCode.A:
                    return CorsairLedId.A;
                case KeyCode.B:
                    return CorsairLedId.B;
                case KeyCode.C:
                    return CorsairLedId.C;
                case KeyCode.D:
                    return CorsairLedId.D;
                case KeyCode.E:
                    return CorsairLedId.E;
                case KeyCode.F:
                    return CorsairLedId.F;
                case KeyCode.G:
                    return CorsairLedId.G;
                case KeyCode.H:
                    return CorsairLedId.H;
                case KeyCode.I:
                    return CorsairLedId.I;
                case KeyCode.J:
                    return CorsairLedId.J;
                case KeyCode.K:
                    return CorsairLedId.K;
                case KeyCode.L:
                    return CorsairLedId.L;
                case KeyCode.M:
                    return CorsairLedId.M;
                case KeyCode.N:
                    return CorsairLedId.N;
                case KeyCode.O:
                    return CorsairLedId.O;
                case KeyCode.P:
                    return CorsairLedId.P;
                case KeyCode.Q:
                    return CorsairLedId.Q;
                case KeyCode.R:
                    return CorsairLedId.R;
                case KeyCode.S:
                    return CorsairLedId.S;
                case KeyCode.T:
                    return CorsairLedId.T;
                case KeyCode.U:
                    return CorsairLedId.U;
                case KeyCode.V:
                    return CorsairLedId.V;
                case KeyCode.W:
                    return CorsairLedId.W;
                case KeyCode.X:
                    return CorsairLedId.X;
                case KeyCode.Y:
                    return CorsairLedId.Y;
                case KeyCode.Z:
                    return CorsairLedId.Z;
                case KeyCode.LeftCurlyBracket:
                    return CorsairLedId.BracketLeft;
                case KeyCode.Pipe:
                    return CorsairLedId.Backslash;
                case KeyCode.RightCurlyBracket:
                    return CorsairLedId.BracketRight;
                case KeyCode.Tilde:
                    return CorsairLedId.GraveAccentAndTilde;
                case KeyCode.Numlock:
                    return CorsairLedId.NumLock;
                case KeyCode.CapsLock:
                    return CorsairLedId.CapsLock;
                case KeyCode.ScrollLock:
                    return CorsairLedId.ScrollLock;
                case KeyCode.RightShift:
                    return CorsairLedId.RightShift;
                case KeyCode.LeftShift:
                    return CorsairLedId.LeftShift;
                case KeyCode.RightControl:
                    return CorsairLedId.RightCtrl;
                case KeyCode.LeftControl:
                    return CorsairLedId.LeftCtrl;
                case KeyCode.RightAlt:
                    return CorsairLedId.RightAlt;
                case KeyCode.LeftAlt:
                    return CorsairLedId.LeftAlt;
                case KeyCode.LeftCommand:
                    return CorsairLedId.LeftGui;
                case KeyCode.LeftWindows:
                    return CorsairLedId.LeftGui;
                case KeyCode.RightCommand:
                    return CorsairLedId.RightGui;
                case KeyCode.RightWindows:
                    return CorsairLedId.RightGui;
                case KeyCode.AltGr:
                    return CorsairLedId.RightAlt;
                case KeyCode.Help:
                    break;
                case KeyCode.Print:
                    return CorsairLedId.PrintScreen;
                case KeyCode.SysReq:
                    return CorsairLedId.PrintScreen;
                case KeyCode.Break:
                    return CorsairLedId.PauseBreak;
                case KeyCode.Menu:
                    return CorsairLedId.Application;
                case KeyCode.Mouse0:
                    break;
                case KeyCode.Mouse1:
                    break;
                case KeyCode.Mouse2:
                    break;
                case KeyCode.Mouse3:
                    break;
                case KeyCode.Mouse4:
                    break;
                case KeyCode.Mouse5:
                    break;
                case KeyCode.Mouse6:
                    break;
                case KeyCode.JoystickButton0:
                    break;
                case KeyCode.JoystickButton1:
                    break;
                case KeyCode.JoystickButton2:
                    break;
                case KeyCode.JoystickButton3:
                    break;
                case KeyCode.JoystickButton4:
                    break;
                case KeyCode.JoystickButton5:
                    break;
                case KeyCode.JoystickButton6:
                    break;
                case KeyCode.JoystickButton7:
                    break;
                case KeyCode.JoystickButton8:
                    break;
                case KeyCode.JoystickButton9:
                    break;
                case KeyCode.JoystickButton10:
                    break;
                case KeyCode.JoystickButton11:
                    break;
                case KeyCode.JoystickButton12:
                    break;
                case KeyCode.JoystickButton13:
                    break;
                case KeyCode.JoystickButton14:
                    break;
                case KeyCode.JoystickButton15:
                    break;
                case KeyCode.JoystickButton16:
                    break;
                case KeyCode.JoystickButton17:
                    break;
                case KeyCode.JoystickButton18:
                    break;
                case KeyCode.JoystickButton19:
                    break;
                case KeyCode.Joystick1Button0:
                    break;
                case KeyCode.Joystick1Button1:
                    break;
                case KeyCode.Joystick1Button2:
                    break;
                case KeyCode.Joystick1Button3:
                    break;
                case KeyCode.Joystick1Button4:
                    break;
                case KeyCode.Joystick1Button5:
                    break;
                case KeyCode.Joystick1Button6:
                    break;
                case KeyCode.Joystick1Button7:
                    break;
                case KeyCode.Joystick1Button8:
                    break;
                case KeyCode.Joystick1Button9:
                    break;
                case KeyCode.Joystick1Button10:
                    break;
                case KeyCode.Joystick1Button11:
                    break;
                case KeyCode.Joystick1Button12:
                    break;
                case KeyCode.Joystick1Button13:
                    break;
                case KeyCode.Joystick1Button14:
                    break;
                case KeyCode.Joystick1Button15:
                    break;
                case KeyCode.Joystick1Button16:
                    break;
                case KeyCode.Joystick1Button17:
                    break;
                case KeyCode.Joystick1Button18:
                    break;
                case KeyCode.Joystick1Button19:
                    break;
                case KeyCode.Joystick2Button0:
                    break;
                case KeyCode.Joystick2Button1:
                    break;
                case KeyCode.Joystick2Button2:
                    break;
                case KeyCode.Joystick2Button3:
                    break;
                case KeyCode.Joystick2Button4:
                    break;
                case KeyCode.Joystick2Button5:
                    break;
                case KeyCode.Joystick2Button6:
                    break;
                case KeyCode.Joystick2Button7:
                    break;
                case KeyCode.Joystick2Button8:
                    break;
                case KeyCode.Joystick2Button9:
                    break;
                case KeyCode.Joystick2Button10:
                    break;
                case KeyCode.Joystick2Button11:
                    break;
                case KeyCode.Joystick2Button12:
                    break;
                case KeyCode.Joystick2Button13:
                    break;
                case KeyCode.Joystick2Button14:
                    break;
                case KeyCode.Joystick2Button15:
                    break;
                case KeyCode.Joystick2Button16:
                    break;
                case KeyCode.Joystick2Button17:
                    break;
                case KeyCode.Joystick2Button18:
                    break;
                case KeyCode.Joystick2Button19:
                    break;
                case KeyCode.Joystick3Button0:
                    break;
                case KeyCode.Joystick3Button1:
                    break;
                case KeyCode.Joystick3Button2:
                    break;
                case KeyCode.Joystick3Button3:
                    break;
                case KeyCode.Joystick3Button4:
                    break;
                case KeyCode.Joystick3Button5:
                    break;
                case KeyCode.Joystick3Button6:
                    break;
                case KeyCode.Joystick3Button7:
                    break;
                case KeyCode.Joystick3Button8:
                    break;
                case KeyCode.Joystick3Button9:
                    break;
                case KeyCode.Joystick3Button10:
                    break;
                case KeyCode.Joystick3Button11:
                    break;
                case KeyCode.Joystick3Button12:
                    break;
                case KeyCode.Joystick3Button13:
                    break;
                case KeyCode.Joystick3Button14:
                    break;
                case KeyCode.Joystick3Button15:
                    break;
                case KeyCode.Joystick3Button16:
                    break;
                case KeyCode.Joystick3Button17:
                    break;
                case KeyCode.Joystick3Button18:
                    break;
                case KeyCode.Joystick3Button19:
                    break;
                case KeyCode.Joystick4Button0:
                    break;
                case KeyCode.Joystick4Button1:
                    break;
                case KeyCode.Joystick4Button2:
                    break;
                case KeyCode.Joystick4Button3:
                    break;
                case KeyCode.Joystick4Button4:
                    break;
                case KeyCode.Joystick4Button5:
                    break;
                case KeyCode.Joystick4Button6:
                    break;
                case KeyCode.Joystick4Button7:
                    break;
                case KeyCode.Joystick4Button8:
                    break;
                case KeyCode.Joystick4Button9:
                    break;
                case KeyCode.Joystick4Button10:
                    break;
                case KeyCode.Joystick4Button11:
                    break;
                case KeyCode.Joystick4Button12:
                    break;
                case KeyCode.Joystick4Button13:
                    break;
                case KeyCode.Joystick4Button14:
                    break;
                case KeyCode.Joystick4Button15:
                    break;
                case KeyCode.Joystick4Button16:
                    break;
                case KeyCode.Joystick4Button17:
                    break;
                case KeyCode.Joystick4Button18:
                    break;
                case KeyCode.Joystick4Button19:
                    break;
                case KeyCode.Joystick5Button0:
                    break;
                case KeyCode.Joystick5Button1:
                    break;
                case KeyCode.Joystick5Button2:
                    break;
                case KeyCode.Joystick5Button3:
                    break;
                case KeyCode.Joystick5Button4:
                    break;
                case KeyCode.Joystick5Button5:
                    break;
                case KeyCode.Joystick5Button6:
                    break;
                case KeyCode.Joystick5Button7:
                    break;
                case KeyCode.Joystick5Button8:
                    break;
                case KeyCode.Joystick5Button9:
                    break;
                case KeyCode.Joystick5Button10:
                    break;
                case KeyCode.Joystick5Button11:
                    break;
                case KeyCode.Joystick5Button12:
                    break;
                case KeyCode.Joystick5Button13:
                    break;
                case KeyCode.Joystick5Button14:
                    break;
                case KeyCode.Joystick5Button15:
                    break;
                case KeyCode.Joystick5Button16:
                    break;
                case KeyCode.Joystick5Button17:
                    break;
                case KeyCode.Joystick5Button18:
                    break;
                case KeyCode.Joystick5Button19:
                    break;
                case KeyCode.Joystick6Button0:
                    break;
                case KeyCode.Joystick6Button1:
                    break;
                case KeyCode.Joystick6Button2:
                    break;
                case KeyCode.Joystick6Button3:
                    break;
                case KeyCode.Joystick6Button4:
                    break;
                case KeyCode.Joystick6Button5:
                    break;
                case KeyCode.Joystick6Button6:
                    break;
                case KeyCode.Joystick6Button7:
                    break;
                case KeyCode.Joystick6Button8:
                    break;
                case KeyCode.Joystick6Button9:
                    break;
                case KeyCode.Joystick6Button10:
                    break;
                case KeyCode.Joystick6Button11:
                    break;
                case KeyCode.Joystick6Button12:
                    break;
                case KeyCode.Joystick6Button13:
                    break;
                case KeyCode.Joystick6Button14:
                    break;
                case KeyCode.Joystick6Button15:
                    break;
                case KeyCode.Joystick6Button16:
                    break;
                case KeyCode.Joystick6Button17:
                    break;
                case KeyCode.Joystick6Button18:
                    break;
                case KeyCode.Joystick6Button19:
                    break;
                case KeyCode.Joystick7Button0:
                    break;
                case KeyCode.Joystick7Button1:
                    break;
                case KeyCode.Joystick7Button2:
                    break;
                case KeyCode.Joystick7Button3:
                    break;
                case KeyCode.Joystick7Button4:
                    break;
                case KeyCode.Joystick7Button5:
                    break;
                case KeyCode.Joystick7Button6:
                    break;
                case KeyCode.Joystick7Button7:
                    break;
                case KeyCode.Joystick7Button8:
                    break;
                case KeyCode.Joystick7Button9:
                    break;
                case KeyCode.Joystick7Button10:
                    break;
                case KeyCode.Joystick7Button11:
                    break;
                case KeyCode.Joystick7Button12:
                    break;
                case KeyCode.Joystick7Button13:
                    break;
                case KeyCode.Joystick7Button14:
                    break;
                case KeyCode.Joystick7Button15:
                    break;
                case KeyCode.Joystick7Button16:
                    break;
                case KeyCode.Joystick7Button17:
                    break;
                case KeyCode.Joystick7Button18:
                    break;
                case KeyCode.Joystick7Button19:
                    break;
                case KeyCode.Joystick8Button0:
                    break;
                case KeyCode.Joystick8Button1:
                    break;
                case KeyCode.Joystick8Button2:
                    break;
                case KeyCode.Joystick8Button3:
                    break;
                case KeyCode.Joystick8Button4:
                    break;
                case KeyCode.Joystick8Button5:
                    break;
                case KeyCode.Joystick8Button6:
                    break;
                case KeyCode.Joystick8Button7:
                    break;
                case KeyCode.Joystick8Button8:
                    break;
                case KeyCode.Joystick8Button9:
                    break;
                case KeyCode.Joystick8Button10:
                    break;
                case KeyCode.Joystick8Button11:
                    break;
                case KeyCode.Joystick8Button12:
                    break;
                case KeyCode.Joystick8Button13:
                    break;
                case KeyCode.Joystick8Button14:
                    break;
                case KeyCode.Joystick8Button15:
                    break;
                case KeyCode.Joystick8Button16:
                    break;
                case KeyCode.Joystick8Button17:
                    break;
                case KeyCode.Joystick8Button18:
                    break;
                case KeyCode.Joystick8Button19:
                    break;
            }
            return CorsairLedId.Invalid;
        }
    }
}
