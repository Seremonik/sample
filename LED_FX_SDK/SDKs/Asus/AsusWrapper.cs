﻿using AsusAuraWrapper;
using LED_FX_SDK.Core;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Asus
{
    public class AsusWrapper : I_LED_SDK_Wrapper
    {
        private AsusAuraService auraService;
        private bool isInitialized = false;
        private bool keysChanges = false;
        private Dictionary<KeyCode, Color> keyColors = new Dictionary<KeyCode, Color>();

        private ConcurrentExclusiveSchedulerPair scheduler;

        #region Public methods

        public void Initialize(Action OnSuccess, Action<LED_SDK_Exception> OnError)
        {
            try
            {
                //InitializeTask(OnSuccess, OnError);
                scheduler = new ConcurrentExclusiveSchedulerPair();
                Task.Factory.StartNew(() => { InitializeTask(OnSuccess, OnError); }, CancellationToken.None, TaskCreationOptions.DenyChildAttach, scheduler.ExclusiveScheduler);

                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, ""));
                return;
            }
            catch(Exception e)
            {
                auraService = null;
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Asus SDK initalized with errors: " + e.ToString()));
                return;
            }
        }

        public LED_SDK_Exception Release()
        {
            if(IsInitialized())
            {
                try
                {
                    //ReleaseTask();
                    Task.Factory.StartNew(ReleaseTask, CancellationToken.None, TaskCreationOptions.DenyChildAttach, scheduler.ExclusiveScheduler);
                }
                catch(Exception e)
                {
                    return new LED_SDK_Exception(LED_SDK_ERROR_CODE.UNKNOWN_ERROR, "Asus SDK Threw exception during Release: " + e.ToString());
                }
            }

            return new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, "");
        }

        public bool IsInitialized()
        {
            return isInitialized;
        }

        public void Refresh()
        {
            if(keysChanges)
            {
                //SetKeyTask();
                Task.Factory.StartNew(SetKeyTask, CancellationToken.None, TaskCreationOptions.DenyChildAttach, scheduler.ExclusiveScheduler);
                keysChanges = false;
            }
        }

        public void SetGlobalColor(Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    //SetGlobalColorTask(color);
                    Task.Factory.StartNew(() => { SetGlobalColorTask(color); }, CancellationToken.None, TaskCreationOptions.DenyChildAttach, scheduler.ExclusiveScheduler);
                }
                catch(Exception e)
                {
                    Debug.LogWarning(e);
                }
            }
        }

        public void SetKeyColor(KeyCode keyCode, Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    lock(keyColors)
                    {
                        keyColors[keyCode] = color;
                    }
                    keysChanges = true;
                }
                catch(Exception e)
                {
                    Debug.LogWarning(e);
                }
            }
        }

        #endregion

        #region Private methods

        private void InitializeTask(Action OnSuccess, Action<LED_SDK_Exception> OnError)
        {
            try
            {
                if(auraService == null)
                {
                    auraService = new AsusAuraService();
                    auraService.EnterSDKMode();
                }
                isInitialized = true;
                OnSuccess();
                SetGlobalColorTask(Color.black);
            }
            catch(Exception)
            {
                auraService = null;
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_UNAVAILABLE, "ASUS SDK not available"));
            }
        }

        private void ReleaseTask()
        {
            try
            {
                lock(auraService)
                {
                    auraService.ExitSDKMode();
                    auraService = null;
                }

                isInitialized = false;
            }
            catch(Exception e)
            {
                Debug.LogWarning(e);
            }
        }

        private void SetGlobalColorTask(Color color)
        {
            try
            {
                ColorUint colorUint = new ColorUint(color);
                lock(auraService)
                {
                    auraService.SetAllLightsToColor(colorUint.r, colorUint.g, colorUint.b, colorUint.a);
                }
            }
            catch(Exception e)
            {
                Debug.LogWarning(e);
            }
        }

        private void SetKeyTask()
        {
            try
            {
                lock(keyColors)
                {
                    foreach(var keyColor in keyColors)
                    {
                        ColorUint colorUint = new ColorUint(keyColor.Value);

                        lock(auraService)
                        {
                            auraService.UpdateDevKeyLightRGBToKeyCode(AsusAuraService.DEVTYPE_KEYBOARD, keyColor.Key, colorUint.r, colorUint.g, colorUint.b, colorUint.a);
                            auraService.UpdateDevKeyLightRGBToKeyCode(AsusAuraService.DEVTYPE_NBKEYBOARD, keyColor.Key, colorUint.r, colorUint.g, colorUint.b, colorUint.a);
                        }
                    }

                    keyColors.Clear();
                }
            }
            catch(Exception e)
            {
                Debug.LogWarning(e);
            }
        }

        #endregion
    }
}
