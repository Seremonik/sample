﻿using LED_FX_SDK.Core;
using Spectral;
using System;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Logitech
{
    public class LogitechWrapper : I_LED_SDK_Wrapper
    {
        private bool isInitialized;

        #region Public methods

        public void Initialize(Action OnSuccess, Action<LED_SDK_Exception> OnError)
        {
            try
            {
                Led.Initialize();
            }
            catch(Exception e)
            {
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Logitech SDK initalized with errors: " + e.ToString()));
                return;
            }

            if(Led.IsInitialized() && Led.LogitechIsEnabled())
            {
                isInitialized = true;
                OnSuccess();
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, ""));
                return;
            }
            else
            {
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_UNAVAILABLE, "Logitech SDK not available on the PC"));
                return;
            }
        }

        public LED_SDK_Exception Release()
        {
            if(IsInitialized())
            {
                try
                {
                    Led.Shutdown();
                }
                catch(Exception e)
                {
                    return new LED_SDK_Exception(LED_SDK_ERROR_CODE.UNKNOWN_ERROR, "Logitech Spectral Wrapper Threw exception during Release: " + e.ToString());
                }
            }

            return new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, "");
        }

        public bool IsInitialized()
        {
            return isInitialized;
        }

        public void Refresh()
        {
            // Logitech updates with each color change.
        }

        public void SetGlobalColor(Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    Led.SetColor(color);
                }
                catch(Exception)
                {
                }
            }
        }

        public void SetKeyColor(KeyCode keyCode, Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    Led.SetColorForLed(keyCode.ToLedName(), color);
                }
                catch(Exception)
                {
                }
            }
        }

        #endregion
    }
}
