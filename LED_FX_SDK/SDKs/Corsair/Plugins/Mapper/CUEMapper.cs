﻿using System;
using System.Runtime.InteropServices;

namespace LED_FX_SDK.SDKs.Corsair.Mapper
{

    public class CUEMapper
    {
        #region Properties
        public const string DLL_NAME = "CUESDK";
        #endregion

        [DllImport(DLL_NAME)]
        public static extern CorsairProtocolDetails CorsairPerformProtocolHandshake();

        [DllImport(DLL_NAME)]
        public static extern int CorsairGetDeviceCount();

        [DllImport(DLL_NAME)]
        public static extern bool CorsairSetLedsColors(int size, IntPtr ledsColors);

        [DllImport(DLL_NAME)]
        public static extern bool CorsairSetLedsColorsBufferByDeviceIndex(int deviceIndex, int size, IntPtr ledsColors);

        [DllImport(DLL_NAME)]
        public static extern bool CorsairGetLedsColors(int size, IntPtr ledsColors);

        [DllImport(DLL_NAME)]
        private static extern IntPtr CorsairGetDeviceInfo(int deviceIndex);

        [DllImport(DLL_NAME)]
        private static extern IntPtr CorsairGetLedPositions();

        [DllImport(DLL_NAME)]
        public static extern bool CorsairSetLedsColorsFlushBuffer();

        [DllImport(DLL_NAME)]
        private static extern IntPtr CorsairGetLedPositionsByDeviceIndex(int deviceIndex);

        [DllImport(DLL_NAME)]
        public static extern CorsairLedId CorsairGetLedIdForKeyName(char keyName);

        [DllImport(DLL_NAME)]
        public static extern bool CorsairRequestControl(CorsairAccessMode accessMode);

        [DllImport(DLL_NAME)]
        public static extern bool CorsairReleaseControl(CorsairAccessMode accessMode);

        [DllImport(DLL_NAME)]
        public static extern CorsairError CorsairGetLastError();
        [DllImport(DLL_NAME)]
        public static extern bool CorsairRegisterKeypressCallback(IntPtr callback, IntPtr context);

        public static CorsairDeviceInfo GetDeviceInfo(int deviceIndex)
        {
            return (CorsairDeviceInfo)Marshal.PtrToStructure(CorsairGetDeviceInfo(deviceIndex), typeof(CorsairDeviceInfo));
        }

        public static CorsairLedPositions GetLedPositions()
        {
            return (CorsairLedPositions)Marshal.PtrToStructure(CorsairGetLedPositions(), typeof(CorsairLedPositions));
        }

        public static CorsairLedPositions GetLedPositionsByDeviceIndex(int deviceIndex)
        {
            return (CorsairLedPositions)Marshal.PtrToStructure(CorsairGetLedPositionsByDeviceIndex(deviceIndex), typeof(CorsairLedPositions));
        }
    }
}
