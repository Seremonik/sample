﻿using AlienFXManagedWrapper;
using LED_FX_SDK.Core;
using LED_FX_SDK.SDKs.Alienware.Extensions;
using System;
using System.Text;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Alienware
{
    public class AlienwareWrapper : I_LED_SDK_Wrapper
    {
        private bool isInitialized = false;

        #region Public methods

        public void Initialize(Action OnSuccess, Action<LED_SDK_Exception> OnError)
        {
            LFX_Result result;
            try
            {
                result = AlienFXWrapperMap.LFX_Initialize();
            }
            catch(Exception e)
            {
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Alienware SDK Couldnt be initialized: " + e.ToString()));
                return;
            }

            if(result == LFX_Result.LFX_Success)
            {
                try
                {
                    AlienFXWrapperMap.LFX_GetNumDevices(out uint deviceNumber);
                    isInitialized = deviceNumber > 0;
                    if(isInitialized)
                    {
                        OnSuccess();
                    }
                }
                catch(Exception e)
                {
                    OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.UNKNOWN_ERROR, "Alienware SDK threw error on Get device: " + e.ToString()));
                    return;
                }

                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, ""));
                return;
            }
            else if(result == LFX_Result.LFX_Error_NoInit || result == LFX_Result.LFX_Failure)
            {
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Alienware SDK Couldnt be initialized"));
                return;
            }
            else if(result == LFX_Result.LFX_Error_NoDevs)
            {
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.NO_DEVICES, "No Alienware devices found"));
                return;
            }
            OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.UNKNOWN_ERROR, "Alienware SDK Couldnt be initialized"));
        }

        public bool IsInitialized()
        {
            return isInitialized;
        }

        public LED_SDK_Exception Release()
        {
            if(IsInitialized())
            {
                try
                {
                    AlienFXWrapperMap.LFX_Release();
                }
                catch(Exception e)
                {
                    return new LED_SDK_Exception(LED_SDK_ERROR_CODE.UNKNOWN_ERROR, "Alienware SDK Threw exception during Release" + e.ToString());
                }
            }
            return new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK, "");
        }


        public void SetGlobalColor(Color color)
        {
            if(IsInitialized())
            {
                try
                {
                    LFX_Result result = AlienFXWrapperMap.LFX_Light(LFX_Position.LFX_All, color.ToUint());
                }
                catch(Exception) { }
            }
        }

        public void SetKeyColor(KeyCode keyCode, Color color)
        {
            // Not supported on Alienware
        }

        public void Refresh()
        {
            if(IsInitialized())
            {
                try
                {
                    LFX_Result result = AlienFXWrapperMap.LFX_Update();
                }
                catch(Exception) { }
            }
        }

        #endregion

        #region Private Methods

        private void ListDevices()
        {
            uint devicesCount;
            uint lights;
            AlienFXWrapperMap.LFX_GetNumDevices(out devicesCount);
            StringBuilder descriptionBuilder = new StringBuilder();
            StringBuilder tempBuilder = new StringBuilder(100);
            LFX_DeviceType device;
            LFX_Position pos;

            for(uint i = 0; i < devicesCount; i++)
            {
                tempBuilder.Clear();
                descriptionBuilder.Clear();
                AlienFXWrapperMap.LFX_GetDeviceDescription(i, tempBuilder, 100, out device);
                descriptionBuilder.Append(tempBuilder + "\n Device Type: " + device + "\n");
                tempBuilder.Clear();
                AlienFXWrapperMap.LFX_GetNumLights(i, out lights);
                for(uint j = 0; j < lights; j++)
                {
                    AlienFXWrapperMap.LFX_GetLightDescription(i, j, tempBuilder, 100);
                    AlienFXWrapperMap.LFX_GetLightLocation(i, j, out pos);
                    descriptionBuilder.AppendLine(j + ": " + tempBuilder + "   Pos: " + pos);
                }
            }
        }

        #endregion
    }
}
