﻿using UnityEngine;

namespace LED_FX_SDK.SDKs.Asus
{
    public struct ColorUint
    {
        public uint r;
        public uint g;
        public uint b;
        public uint a;

        public ColorUint(Color color)
        {
            r = (uint)(color.r * 255);
            g = (uint)(color.g * 255);
            b = (uint)(color.b * 255);
            a = (uint)(color.a * 255);
        }
    }
}