﻿using UnityEngine;

namespace LED_FX_SDK.Core
{
    /// <summary>
    /// Extension class only for Syntactic sugar purpose. Instead of LED_FX_Controller.AddAnimation(new FlashAnimation(...)) you can invoke it LED_FX_Controller.Animations.Flash(...)
    /// </summary>
    public static partial class LED_FX_Extensions
    {
        public static LED_FX_Animation Breathe(this LED_FX_Animation_Wrapper animationWrapper, Color color, float frequency)
        {
            return animationWrapper.AddAnimation(new BreatheAnimation(color, frequency));
        }

        public static LED_FX_Animation BreatheKeys(this LED_FX_Animation_Wrapper animationWrapper, Color color, float frequency, params KeyCode[] keysCode)
        {
            return animationWrapper.AddAnimation(new BreatheKeysAnimation(color, frequency, keysCode));
        }
    }

    public class BreatheAnimation : LED_FX_Animation
    {
        private readonly float frequency;
        Color color;

        public BreatheAnimation(Color color, float frequency)
        {
            this.frequency = frequency;
            this.color = color;
            IsLooped = true;
        }

        protected override void Animation(float deltaTime)
        {
            LED_FX_Controller.GlobalColor = (color.SetAlpha((Mathf.Sin(Mathf.PI * ((TotalDuration / frequency) % 2)) + 1) / 2));
        }

        public override void AnimationEnded()
        {
            LED_FX_Controller.ResetToAmbient();
        }
    }

    public class BreatheKeysAnimation : LED_FX_Animation
    {
        private readonly float frequency;
        Color color;
        KeyCode[] keysCode;

        public BreatheKeysAnimation(Color color, float frequency, KeyCode[] keysCode)
        {
            this.keysCode = keysCode;
            this.frequency = frequency;
            this.color = color;
            IsLooped = true;
        }

        protected override void Animation(float deltaTime)
        {
            LED_FX_Controller.SetKeysColor(color.SetAlpha((Mathf.Sin(Mathf.PI * ((TotalDuration / frequency) % 2)) + 1) / 2), keysCode);
        }

        public override void AnimationResumed()
        {
            TotalDuration = 0;
        }

        public override void AnimationPaused()
        {
            LED_FX_Controller.ClearKeys(keysCode);
        }

        public override void AnimationEnded()
        {
            LED_FX_Controller.ClearKeys(keysCode);
        }
    }
}
