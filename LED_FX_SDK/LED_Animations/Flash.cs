﻿using UnityEngine;

namespace LED_FX_SDK.Core
{
    /// <summary>
    /// Extension class only for Syntactic sugar purpose. Instead of LED_FX_Controller.AddAnimation(new FlashAnimation(...)) you can invoke it LED_FX_Controller.Animations.Flash(...)
    /// </summary>
    public static partial class LED_FX_Extensions
    {
        public static LED_FX_Animation Flash(this LED_FX_Animation_Wrapper animationWrapper, Color color, float duration)
        {
            return animationWrapper.AddAnimation(new FlashAnimation(color, duration));
        }

        public static LED_FX_Animation FlashKey(this LED_FX_Animation_Wrapper animationWrapper, Color color, KeyCode keyCode, float duration)
        {
            return animationWrapper.AddAnimation(new FlashKeyAnimation(color, keyCode, duration));
        }
    }

    public class FlashAnimation : LED_FX_Animation
    {
        Color color;
        readonly float totalDuration;

        public FlashAnimation(Color color, float duration)
        {
            this.color = color;
            totalDuration = duration;
            durationLeft = duration;
        }

        protected override void Animation(float deltaTime)
        {
            LED_FX_Controller.GlobalColor = (color.SetAlpha(durationLeft / totalDuration));
        }

        public override void AnimationEnded()
        {
            LED_FX_Controller.ResetToAmbient();
        }
    }

    public class FlashKeyAnimation : LED_FX_Animation
    {
        Color color;
        KeyCode keycode;
        readonly float totalDuration;

        public FlashKeyAnimation(Color color, KeyCode keycode, float duration)
        {
            this.keycode = keycode;
            this.color = color;
            totalDuration = duration;
            durationLeft = duration;
        }

        protected override void Animation(float deltaTime)
        {
            LED_FX_Controller.SetKeysColor(color.SetAlpha(durationLeft / totalDuration), keycode);
        }

        public override void AnimationEnded()
        {
            LED_FX_Controller.ClearKeys(keycode);
        }
    }
}
