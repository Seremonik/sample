﻿using LED_FX_SDK.Core;
using LED_FX_SDK.SDKs.Corsair.Extensions;
using LED_FX_SDK.SDKs.Corsair.Mapper;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnityEngine;

namespace LED_FX_SDK.SDKs.Corsair
{
    public class CorsairWrapper : I_LED_SDK_Wrapper
    {
        private bool isInitialized = false;
        private bool updateGlobalColor = false;
        private bool updateKeysColor = false;
        private int devicesCount;
        private Dictionary<CorsairLedId, CorsairLedColor> savedColors;
        private Dictionary<CorsairLedId, CorsairLedColor> availableLeds;
        private List<CorsairLedColor> keysToUpdate;

        #region Public methods

        public void Initialize(Action OnSuccess, Action<LED_SDK_Exception> OnError)
        {
            try
            {
                var protocolDetails = CUEMapper.CorsairPerformProtocolHandshake();

                if(protocolDetails.IsSDKAvailable)
                {
                    if(protocolDetails.BreakingChanges)
                    {
                        OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_NOT_INITIALIZED, "Corsair SDK Is not compatabile with the installed version of CUE"));
                        return;
                    }

                    devicesCount = CUEMapper.CorsairGetDeviceCount();
                    isInitialized = devicesCount > 0;

                    if(isInitialized)
                    {
                        OnSuccess();
                        availableLeds = new Dictionary<CorsairLedId, CorsairLedColor>();
                        keysToUpdate = new List<CorsairLedColor>();

                        ListAllLeds();
                        SaveColors();
                    }
                    else
                    {
                        CUEMapper.CorsairReleaseControl(CorsairAccessMode.ExclusiveLightingControl);
                    }
                    OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.OK));
                    return;
                }
                else
                {
                    OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_UNAVAILABLE, "Corsair SDK not available on system"));
                    return;
                }
            }
            catch(Exception e)
            {
                OnError(new LED_SDK_Exception(LED_SDK_ERROR_CODE.SDK_UNAVAILABLE, "Corsair SDK threw exception during initialization: " + e));
            }
        }

        public LED_SDK_Exception Release()
        {
            if(IsInitialized())
            {
                RestoreOriginalColors();
                CUEMapper.CorsairReleaseControl(CorsairAccessMode.ExclusiveLightingControl);
            }

            return new LED_SDK_Exception();
        }

        public bool IsInitialized()
        {
            return isInitialized;
        }

        public void Refresh()
        {
            if(IsInitialized())
            {
                if(updateGlobalColor)
                {
                    updateGlobalColor = false;
                    UpdateColorsForList(availableLeds.Count, availableLeds.Values);
                }

                if(updateKeysColor)
                {
                    updateKeysColor = false;
                    UpdateColorsForList(keysToUpdate.Count, keysToUpdate);
                    keysToUpdate.Clear();
                }
            }
        }

        public void SetGlobalColor(Color color)
        {
            if(IsInitialized())
            {
                foreach(var led in availableLeds)
                {
                    UpdateCorsairColor(led.Value, color);
                }

                updateGlobalColor = true;
            }
        }

        public void SetKeyColor(KeyCode keyCode, Color color)
        {
            if(IsInitialized())
            {
                if(!keysToUpdate.Contains(availableLeds[keyCode.ToCorsairLedId()]))
                {
                    keysToUpdate.Add(availableLeds[keyCode.ToCorsairLedId()]);
                }

                UpdateCorsairColor(availableLeds[keyCode.ToCorsairLedId()], color);
                updateKeysColor = true;
            }
        }

        #endregion

        #region Private methods

        private void ListAllLeds()
        {
            availableLeds.Clear();

            try
            {
                for(int i = 0; i < CUEMapper.CorsairGetDeviceCount(); i++)
                {
                    CorsairLedPositions leds = CUEMapper.GetLedPositionsByDeviceIndex(i);
                    bool success = CUEMapper.CorsairGetLastError() == CorsairError.Success;

                    if(success)
                    {
                        foreach(var led in leds.LedPosition)
                        {
                            availableLeds.Add(led.ledId, new CorsairLedColor() { ledId = (int)led.ledId, r = 0, g = 0, b = 0 });
                        }
                    }
                }
            }
            catch(Exception)
            {
            }
        }

        private void SaveColors()
        {
            try
            {
                savedColors = new Dictionary<CorsairLedId, CorsairLedColor>();

                int structSize = Marshal.SizeOf(typeof(CorsairLedColor));
                IntPtr ptr = Marshal.AllocHGlobal(structSize * availableLeds.Count);
                IntPtr addPtr = new IntPtr(ptr.ToInt64());
                foreach(var led in availableLeds)
                {
                    CorsairLedColor color = new CorsairLedColor { ledId = (int)led.Key };
                    Marshal.StructureToPtr(color, addPtr, false);
                    addPtr = new IntPtr(addPtr.ToInt64() + structSize);
                }

                CUEMapper.CorsairGetLedsColors(availableLeds.Count, ptr);

                IntPtr readPtr = ptr;
                savedColors.Clear();
                for(int i = 0; i < availableLeds.Count; i++)
                {
                    CorsairLedColor ledColor = (CorsairLedColor)Marshal.PtrToStructure(readPtr, typeof(CorsairLedColor));
                    savedColors.Add((CorsairLedId)ledColor.ledId, ledColor);

                    readPtr = new IntPtr(readPtr.ToInt64() + structSize);
                }

                Marshal.FreeHGlobal(ptr);
            }
            catch(Exception)
            {
            }
        }

        private void RestoreOriginalColors()
        {
            availableLeds = savedColors;
            UpdateColorsForList(availableLeds.Values.Count, availableLeds.Values);
        }

        private void UpdateColorsForList(int size, IEnumerable<CorsairLedColor> list)
        {
            if(size > 0)
            {
                try
                {
                    int structSize = Marshal.SizeOf(typeof(CorsairLedColor));
                    IntPtr ptr = Marshal.AllocHGlobal(structSize * size);
                    IntPtr addPtr = new IntPtr(ptr.ToInt64());

                    foreach(var keyColor in list)
                    {
                        Marshal.StructureToPtr(keyColor, addPtr, false);
                        addPtr = new IntPtr(addPtr.ToInt64() + structSize);
                    }

                    Task.Run(() => UpdateColorTask(size, ptr));
                }
                catch(Exception)
                {
                }
            }
        }

        void UpdateColorTask(int count, IntPtr ptr)
        {
            CUEMapper.CorsairSetLedsColors(count, ptr);
            Marshal.FreeHGlobal(ptr);
        }

        private void UpdateCorsairColor(CorsairLedColor corsairLedColor, Color unityColor)
        {
            corsairLedColor.r = (byte)(unityColor.r * 255 * unityColor.a);
            corsairLedColor.g = (byte)(unityColor.g * 255 * unityColor.a);
            corsairLedColor.b = (byte)(unityColor.b * 255 * unityColor.a);
        }

        #endregion
    }
}
