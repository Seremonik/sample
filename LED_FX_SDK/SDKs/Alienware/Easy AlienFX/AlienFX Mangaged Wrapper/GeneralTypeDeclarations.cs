﻿using System.Runtime.InteropServices;

namespace AlienFXManagedWrapper
{
    public enum LFX_Result
    {
        LFX_Success,
        LFX_Failure,
        LFX_Error_NoInit,
        LFX_Error_NoDevs,
        LFX_Error_NoLights,
        LFX_Error_BuffSize
    }

    public enum LFX_Position : uint
    {
        LFX_Front_Lower_Left = 0x00000001,
        LFX_Front_Lower_Center = 0x00000002,
        LFX_Front_Lower_Right = 0x00000004,
        LFX_Front_Middle_Left = 0x00000008,
        LFX_Front_Middle_Center = 0x00000010,
        LFX_Front_Middle_Right = 0x00000020,
        LFX_Front_Upper_Left = 0x00000040,
        LFX_Front_Upper_Center = 0x00000080,
        LFX_Front_Upper_Right = 0x00000100,
        LFX_Middle_Lower_Left = 0x00000200,
        LFX_Middle_Lower_Center = 0x00000400,
        LFX_Middle_Lower_Right = 0x00000800,
        LFX_Middle_Middle_Left = 0x00001000,
        LFX_Middle_Middle_Center = 0x00002000,
        LFX_Middle_Middle_Right = 0x00004000,
        LFX_Middle_Upper_Left = 0x00008000,
        LFX_Middle_Upper_Center = 0x00010000,
        LFX_Middle_Upper_Right = 0x00020000,
        LFX_Rear_Lower_Left = 0x00040000,
        LFX_Rear_Lower_Center = 0x00080000,
        LFX_Rear_Lower_Right = 0x00100000,
        LFX_Rear_Middle_Left = 0x00200000,
        LFX_Rear_Middle_Center = 0x00400000,
        LFX_Rear_Middle_Right = 0x00800000,
        LFX_Rear_Upper_Left = 0x01000000,
        LFX_Rear_Upper_Center = 0x02000000,
        LFX_Rear_Upper_Right = 0x04000000,
        LFX_All = 0x07FFFFFF,
        LFX_All_Right = 0x04924924,
        LFX_All_Left = 0x01249249,
        LFX_All_Upper = 0x070381C0,
        LFX_All_Lower = 0x001C0E07,
        LFX_All_Front = 0x000001FF,
        LFX_All_Rear = 0x07FC0000
    }

    public enum LFX_DeviceType : ushort
    {
        LFX_DevType_Unknown = 0x00,
        LFX_DevType_Notebook = 0x01,
        LFX_DevType_Desktop = 0x02,
        LFX_DevType_Server = 0x03,
        LFX_DevType_Display = 0x04,
        LFX_DevType_Mouse = 0x05,
        LFX_DevType_Keyboard = 0x06,
        LFX_DevType_Gamepad = 0x07,
        LFX_DevType_Speaker = 0x08,
        LFX_DevType_Custom = 0xFE,
        LFX_DevType_Other = 0xFF
    }

    public struct LFX_ColorStruct
    {
        public byte red;
        public byte green;
        public byte blue;
        public byte brightness;
    }
}
